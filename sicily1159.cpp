//题目网址:
//http://soj.me/1159

//题目分析:
//同样是高精度加法，分析同1201

#include <iostream>
#include <string>

using namespace std;

int main()
{
    int rsl[110] = {0};
    int index;
    bool flag = false;
    int N;
    int length;
    while (cin >> N) {  
        length = 109;
        for (int i = 0; i < N; i++) {
            string str;
            cin >> str;
            index = 109;
            flag = false;
            for (int l = str.length() - 1; l >= 0; l--, index--) {
                rsl[index] += str[l] - '0';
                if (flag) {
                    rsl[index]++;
                    flag = false;
                } 

                if (rsl[index] >= 10) {
                    flag = true;
                    rsl[index] %= 10;
                }
            }
            if (flag)
                rsl[index]++;

            if (index < length)
                length = index;
        }

        index = length;

        if (rsl[index] == 0) { index++; flag = true;}
        while (index <= 109) {
            if (rsl[index] == 0 && flag) {
                index++;
                continue;
            } else if (rsl[index] != 0) {
                flag = false;
            }
            cout << rsl[index];
            rsl[index] = 0;
            index++;
        }
        if (flag) { cout << 0; }
        cout << endl;
    }
    
    return 0;
}                                 